import { GetStaticProps } from "next";
import { getSortedPostsData } from "../../lib/posts";
import { PostAsCard } from "../../components/posts/PostAsCard";
import PageHeader from "../../components/PageHeader";
import Section from "../../components/Section";
import useTranslation from "next-translate/useTranslation";
import getT from "next-translate/getT";

export default function Posts({ posts }: { posts: Post[] }) {
  const { t } = useTranslation("blog");
  return (
    <>
      <PageHeader
        title={t("title")}
        description={t("description")}
      ></PageHeader>
      <Section>
        <div className="max-w-screen-md mx-auto">
          {posts.map((post) => (
            <div className="" key={post.id}>
              <div className="mb-5">
                <PostAsCard {...post} />
              </div>
            </div>
          ))}
        </div>
      </Section>
    </>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const posts = getSortedPostsData();
  const t = await getT(locale, "blog");
  return {
    props: {
      posts,
      title: t("title"),
      description: t("description"),
    },
  };
};
