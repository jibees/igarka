import { getAllPostIds, getPostData } from "../../lib/posts";
import Date from "../../components/date";
import { GetStaticProps, GetStaticPaths } from "next";
import PageHeader from "../../components/PageHeader";
import Image from "next/image";
import useTranslation from "next-translate/useTranslation";

const Post = ({ postData }: { postData: Post }) => {
  const { t, lang } = useTranslation();
  return (
    <>
      <PageHeader
        title={postData.title}
        description={postData.summary}
      ></PageHeader>
      <article className="mx-auto max-w-4xl lg:mt-16 m-5">
        {postData.image && (
          <div className="relative pb-9/16 rel">
            <Image
              layout="fill"
              objectFit="cover"
              className="absolute shadow-lg"
              src={`/images/${postData.image}`}
            />
          </div>
        )}
        <div className="text-gray-500 text-sm italic font-thin leading-normal text-right mt-5">
          <Date dateString={postData.date} lang={lang} />
        </div>
        <div className="mt-5 mb-5 lg:mt-16 lg:mb-20 justify-center bg-white p-5 lg:p-0 relative text-gray-800 lg:text-lg leading-8">
          <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
        </div>
      </article>
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = getAllPostIds();
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const postData = await getPostData(params.id as string);
  return {
    props: {
      postData,
      title: postData.title,
      description: postData.summary,
      image: postData.image ? postData.image : null,
    },
  };
};

export default Post;
