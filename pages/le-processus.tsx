import { GetStaticProps } from "next";
import PageHeader from "../components/PageHeader";
import useTranslation from "next-translate/useTranslation";
import getT from "next-translate/getT";

const Processus = () => {
  const { t, lang } = useTranslation("processus");
  return (
    <>
      <PageHeader
        title={t("title")}
        description={t("description")}
      ></PageHeader>
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const t = await getT(locale, "processus");
  return {
    props: {
      home: false,
      title: t("title"),
      description: t("description"),
    },
  };
};
export default Processus;
