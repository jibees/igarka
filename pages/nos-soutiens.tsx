import { GetStaticProps } from "next";
import PageHeader from "../components/PageHeader";
import useTranslation from "next-translate/useTranslation";
import getT from "next-translate/getT";
import Section from "../components/Section";
import { Title } from "../components/home";

const NosSoutiens = () => {
  const { t, lang } = useTranslation("soutiens");
  return (
    <>
      <PageHeader
        title={t("title")}
        description={t("description")}
      ></PageHeader>
      <Section>
        <Title>{t("axe_title")}</Title>
        <div className="mt-10 lg:mt-20 max-w-screen-md m-auto">
          {t("axe_description")}
        </div>
        <div className="mt-10 lg:mt-20 text-center">
          <a
            className="text-green-700 hover:text-green-800"
            href="https://www.omp.eu/missions/recherche/les-actions-scientifiques-transverses/arctique/"
          >
            {t("axe_link")}
          </a>
        </div>
      </Section>
      <Section>
        <Title>{t("miti_title")}</Title>
        <div className="mt-10 lg:mt-20 max-w-screen-md m-auto">
          {t("miti_description")}
        </div>
        <div className="mt-10 lg:mt-20 text-center">
          <a
            className="text-green-700 hover:text-green-800"
            href="https://miti.cnrs.fr/"
          >
            {t("miti_link")}
          </a>
        </div>
      </Section>
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const t = await getT(locale, "soutiens");
  return {
    props: {
      home: false,
      title: t("title"),
      description: t("description"),
    },
  };
};
export default NosSoutiens;
