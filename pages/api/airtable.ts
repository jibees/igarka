import AirtablePlus from "airtable-plus";

const inst = new AirtablePlus({
  apiKey: process.env.AIRTABLE_API_KEY,
  baseID: process.env.AIRTABLE_BASE_ID,
  tableName: "Email_Subscriptions",
});

export const addEmail = (email: string): Promise<void> =>
  inst.create({
    email: email,
  });
