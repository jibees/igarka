import { NextApiRequest, NextApiResponse } from "next";
import { addEmail } from "./airtable";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "POST":
      const email = req.body.email;

      if (!isValidEmail(email)) {
        return res.status(400).json({ status: 400, message: "Invalid email" });
      }

      try {
        await addEmail(email);
        return res.status(200).json({ status: 200, message: "OK" });
      } catch (err) {
        return res
          .status(400)
          .json({ status: 400, message: err.message, originalError: err });
      }
    default:
      res.setHeader("Allow", ["POST"]);
      return res.status(405).end();
  }
};

const isValidEmail = (email: string): boolean => {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return true;
  }

  return false;
};
