import { GetStaticProps, GetStaticPaths } from "next";
import { getAllProjectMembersId, getMemberData } from "../../lib/members";
import { getFullName, getAvatarPath } from "../../components/members/util";
import PageHeader from "../../components/PageHeader";
import Section from "../../components/Section";
import TwitterIcon from "../../assets/svg/social/TwitterIcon.svg";
import LinkedinIcon from "../../assets/svg/social/LinkedinIcon.svg";
import LinkIcon from "../../assets/svg/social/LinkIcon.svg";
import Arrow from "../../assets/svg/Arrow.svg";
import Link from "next/link";
import fs from "fs";
import Image from "next/image";
import useTranslation from "next-translate/useTranslation";
import getT from "next-translate/getT";

const Membre = ({ member }: { member: ProjectMember }) => {
  const { t } = useTranslation("membres");
  return (
    <>
      <PageHeader title={getFullName(member)}>
        <div className="flex flex-col items-center">
          <div
            className="bg-white absolute inline-flex shadow-lg border border-gray-600 rounded-full overflow-hidden h-40 w-40 z-10"
            style={{
              bottom: "-7rem",
            }}
          >
            <Image
              layout="fill"
              src={getAvatarPath(member)}
              alt=""
              className="w-full object-contain h-48"
            />
          </div>
        </div>
      </PageHeader>

      <Section>
        <div
          className="container mx-auto mt-16 max-w-4xl flex flex-col items-center justify-center bg-white p-4 pt-10 shadow rounded-lg relative"
          key={member.id}
        >
          <h2 className="font-bold text-xl text-light-green-blue">
            {member.job}
          </h2>

          <SocialAccounts member={member} />

          <div
            className="text-gray-600 text-justify mt-10"
            dangerouslySetInnerHTML={{ __html: member.content }}
          />
        </div>

        <div className="mt-20 mx-auto max-w-3xl">
          <div className="flex flex-col items-center ">
            <Link href="/membres">
              <a className="link flex flex-row ">
                <div className="w-3 h-3 mx-3 mt-3 transform rotate-180">
                  <Arrow />
                </div>
                {t("back")}
              </a>
            </Link>
          </div>
        </div>
      </Section>
    </>
  );
};

const SocialAccounts = ({ member }) => {
  const socialCount = [member.twitter, member.linkedin, member.link].filter(
    (social) => social != null
  ).length;

  return (
    <div
      className={`text-center tracking-wide grid grid-cols-${socialCount} gap-6`}
    >
      {member.twitter && (
        <SocialAccount
          href={`https://twitter.com/${member.twitter}`}
          label={member.twitter}
        >
          <TwitterIcon />
        </SocialAccount>
      )}
      {member.linkedin && (
        <SocialAccount
          href={`https://www.linkedin.com/in/${member.linkedin}`}
          label={getFullName(member)}
        >
          <LinkedinIcon />
        </SocialAccount>
      )}
      {member.link && (
        <SocialAccount href={member.link} label={new URL(member.link).hostname}>
          <LinkIcon />
        </SocialAccount>
      )}
    </div>
  );
};

const SocialAccount = ({ href, label, children }) => (
  <a href={href} className="flex items-center mt-4 text-gray-700">
    <div className="h-5 w-5 fill-current relative -top-bottom-align">
      {children}
    </div>
    <span className="px-2 text-sm">{label}</span>
  </a>
);

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = getAllProjectMembersId();
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params, locale }) => {
  const member = await getMemberData(params.id as string);
  return {
    props: {
      member,
      title: getFullName(member),
      description: getOpenGraphDescription(member),
      image: getAvatarName(member),
    },
  };
};

const getAvatarName = (member: ProjectMember): string => {
  const file = `public/images/members/${member.id}.jpg`;
  if (fs.existsSync(file)) {
    return `members/${member.id}.jpg`;
  }
  return "members/default.png";
};

const getOpenGraphDescription = (member: ProjectMember): string => {
  if (member.summary && member.summary.length > 0) {
    return member.summary;
  }
  let description = member.job;
  return description;
};

export default Membre;
