import { GetStaticProps } from "next";
import { getProjectMembers } from "../../lib/members";
import MemberAsCard from "../../components/members/MemberAsCard";
import PageHeader from "../../components/PageHeader";
import Section from "../../components/Section";
import useTranslation from "next-translate/useTranslation";
import getT from "next-translate/getT";

export default function Membres({
  projectMembers,
}: {
  projectMembers: ProjectMember[];
}) {
  const { t } = useTranslation("membres");
  return (
    <>
      <PageHeader
        title={t("membres:title")}
        description={t("membres:description")}
      ></PageHeader>
      <Section>
        <div className="xl:mx-20 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-3 gap-4">
          {projectMembers.map((member) => (
            <MemberAsCard {...member} key={member.id} />
          ))}
        </div>
      </Section>
    </>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const projectMembers = getProjectMembers();
  const t = await getT(locale, "membres");
  return {
    props: {
      projectMembers,
      title: t("title"),
      description: t("description"),
    },
  };
};
