import { GetStaticProps } from "next";
import PageHeader from "../components/PageHeader";
import IgarkaWP from "../assets/svg/igarka-workpackages.svg";
import Section from "../components/Section";
import { Title } from "../components/home";
import { ReactNode } from "react";
import ImageWithModal from "../components/ImageWithModal";
import useTranslation from "next-translate/useTranslation";
import getT from "next-translate/getT";
import Trans from "next-translate/Trans";

const WorkPackages = () => {
  const { t, lang } = useTranslation("spheres");
  return (
    <>
      <PageHeader
        title={t("title")}
        description={t("description")}
      ></PageHeader>
      <Section>{t("first_paragraphe")}</Section>
      <Section>
        <Title>{t("schema_title")}</Title>
        <div className="mt-10 lg:mt-20">
          <IgarkaWP />
        </div>
      </Section>
      <Section>
        <Title>{t("meta_title")}</Title>
        <SubTitle>{t("meta_subtitle")}</SubTitle>
        <div className="text-gray-800 lg:flex lg:items-center lg:mx-20 ">
          <div className="lg:w-1/2 lg:mx-20 mt-10">
            <ImageWithModal name="sphere-meta.jpg"></ImageWithModal>
          </div>
          <div className="lg:w-1/2 text-justify mt-5 lg:mr-10">
            <div className="mb-10 lg:text-right ">
              <List
                items={[t("meta_item_1"), t("meta_item_2"), t("meta_item_3")]}
              />
            </div>
            <Trans i18nKey="spheres:meta_content" components={[<p />]} />
          </div>
        </div>
      </Section>
      <Section>
        <Title>{t("ville_title")}</Title>
        <SubTitle>{t("ville_subtitle")}</SubTitle>
        <div className="text-gray-800 lg:flex lg:items-center lg:mx-20 lg:flex-row-reverse">
          <div className="lg:w-1/2 lg:mx-20 mt-10">
            <ImageWithModal name="sphere-urbaine.jpg"></ImageWithModal>
          </div>
          <div className="lg:w-1/2 text-justify mt-5 lg:mr-10">
            <div className="mb-10">
              <List
                items={[
                  t("meta_item_1"),
                  t("meta_item_2"),
                  t("meta_item_3"),
                  t("meta_item_4"),
                ]}
              />
            </div>
            <Trans i18nKey="spheres:ville_content" components={[<p />]} />
          </div>
        </div>
      </Section>
      <Section>
        <Title>{t("kombinat_title")}</Title>
        <SubTitle>{t("kombinat_subtitle")}</SubTitle>
        <div className="text-gray-800 lg:flex lg:items-center lg:mx-20 ">
          <div className="lg:w-1/2 lg:mx-20 mt-10">
            <ImageWithModal name="sphere-kombinat.jpg"></ImageWithModal>
          </div>
          <div className="lg:w-1/2 text-justify mt-5 lg:mr-10">
            <div className="mb-10 lg:text-right ">
              <List
                items={[
                  t("kombinat_item_1"),
                  t("kombinat_item_2"),
                  t("kombinat_item_3"),
                ]}
              />
            </div>
            <Trans i18nKey="spheres:kombinat_content" components={[<p />]} />
          </div>
        </div>
      </Section>
      <Section>
        <Title>{t("fleuve_title")}</Title>
        <SubTitle>{t("fleuve_subtitle")}</SubTitle>
        <div className="text-gray-800 lg:flex lg:items-center lg:mx-20 lg:flex-row-reverse">
          <div className="lg:w-1/2 lg:mx-20 mt-10">
            <ImageWithModal name="sphere-ienissei.jpg"></ImageWithModal>
          </div>
          <div className="lg:w-1/2 text-justify mt-5 lg:mr-10">
            <div className="mb-10 ">
              <List
                items={[
                  t("fleuve_item_1"),
                  t("fleuve_item_2"),
                  t("fleuve_item_3"),
                ]}
              />
            </div>
            <Trans i18nKey="spheres:fleuve_content" components={[<p />]} />
          </div>
        </div>
      </Section>
    </>
  );
};

const List = ({ items }: { items: string[] }) => (
  <div className="lg:text-xl text-gray-700 font-light mt-5 lg:mt-0">
    {items.map((item) => (
      <div>
        <span className="text-green-800 mr-2 font-bold">✓</span>
        {item}
      </div>
    ))}
  </div>
);

const SubTitle = ({ children }: { children: ReactNode }) => (
  <div className="container mt-5 lg:w-2/3 lg:px-20 italic mx-auto text-center text-gray-700">
    {children}
  </div>
);

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const t = await getT(locale, "spheres");
  return {
    props: {
      home: false,
      title: t("title"),
      description: t("description"),
    },
  };
};
export default WorkPackages;
