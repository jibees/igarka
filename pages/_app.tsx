import "../styles/global.css";
import { AppProps } from "next/app";
import Layout from "../components/layout";

const MyApp = ({ Component, pageProps }: AppProps) => (
  <Layout
    home={pageProps.home}
    title={pageProps.title}
    image={pageProps.image}
    description={pageProps.description}
  >
    <Component {...pageProps} />
  </Layout>
);

export default MyApp;
