import { getSortedPostsData } from "../lib/posts";
import Link from "next/link";
import { GetStaticProps } from "next";
import NewsletterSubscriptionForm from "../components/NewsletterSubscriptionForm";
import Group from "../assets/svg/Group.svg";
import Boxes from "../assets/svg/Boxes.svg";
import Salute from "../assets/svg/Salute.svg";
import { Title, ThirdComponent } from "../components/home";
import { PostAsCard } from "../components/posts/PostAsCard";
import Section from "../components/Section";
import ImageWithModal from "../components/ImageWithModal";
import Image from "next/image";
import useTranslation from "next-translate/useTranslation";
import getT from "next-translate/getT";

interface HomeProps {
  firstThirdPosts: SummaryPost[];
}
const Home = ({ firstThirdPosts }: HomeProps) => {
  const { t, lang } = useTranslation("index");
  return (
    <>
      <header className="w-full bg-center py-40 relative">
        <Image
          alt="banner"
          src="/images/banner.jpg"
          layout="fill"
          objectFit="cover"
          quality={100}
        />
        <div className="container mx-auto relative">
          <div className="flex flex-col items-center p-12">
            <h1 className="font-semibold text-gray-700 uppercase text-6xl tracking-tighter">
              {t("title")}
            </h1>
            <p className="text-lg text-gray-600">{t("subtitle")}</p>
          </div>
        </div>
      </header>

      <Section>
        <div className="lg:py-12 lg:flex lg:justify-center">
          <div className="lg:w-1/2">
            <Title>{t("#1-title")}</Title>
            <div className="md:w-2/3 mx-auto lg:mt-10 mb-5 lb:mb-0">
              <ImageWithModal name="blog/igarka-2.jpg" />
            </div>
          </div>
          <div className="lg:w-1/2 lg:mt-10 xl:mt-20">
            <div className="text-justify text-gray-800 text-lg">
              <p className="lg:mb-5">{t("#1-content-1")}</p>
              <p className="lg:mb-5">{t("#1-content-2")}</p>
              <p className="text-right">
                <Link href="/le-projet">
                  <a className="text-green-700 hover:text-green-800">
                    {t("#1-content-link")}
                  </a>
                </Link>
              </p>
            </div>
          </div>
        </div>
      </Section>
      <Section>
        <Title>{t("#2-title")}</Title>
        <div className="lg:flex lg:mt-10 content-center justify-center">
          <ThirdComponent icon={<Boxes />}>
            <p className="lg:mb-5">{t("#2-content-2-1")}</p>
            <p className="lg:mb-5">{t("#2-content-2-2")}</p>
            <p className="text-center">
              <Link href="/les-spheres-de-recherche">
                <a className="text-green-700 hover:text-green-800">
                  {t("#2-content-2-link")}
                </a>
              </Link>
            </p>
          </ThirdComponent>

          <ThirdComponent
            icon={
              <div className="transform rotate-45">
                <Salute />
              </div>
            }
          >
            <p className="lg:mb-5">{t("#2-content-3-1")}</p>
            <p className="lg:mb-5">{t("#2-content-3-2")}</p>
            <p className="text-center">
              <Link href="/igarka">
                <a className="text-green-700 hover:text-green-800">
                  {t("#2-content-3-link")}
                </a>
              </Link>
            </p>
          </ThirdComponent>

          <ThirdComponent icon={<Group />}>
            <p className="lg:mb-5">{t("#2-content-1-1")}</p>
            <p className="lg:mb-5">{t("#2-content-1-2")}</p>
            <p className="text-center">
              <Link href="/membres">
                <a className="text-green-700 hover:text-green-800">
                  {t("#2-content-1-link")}
                </a>
              </Link>
            </p>
          </ThirdComponent>
        </div>
      </Section>
      <Section>
        <Title>{t("#3-title")}</Title>
        <div
          className={`lg:grid xl:grid-cols-${
            firstThirdPosts.length
          } lg:grid-cols-${firstThirdPosts.length - 1} gap-6 mt-10`}
        >
          {firstThirdPosts.map((post) => (
            <div className="lg:mt-0 mt-5" key={post.id}>
              <PostAsCard {...post} />
            </div>
          ))}
        </div>
        <p className="text-center mt-5">
          <Link href="/blog">
            <a className="text-green-700 hover:text-green-800">
              {t("#3-link")}
            </a>
          </Link>
        </p>
      </Section>
      <Section>
        <div className="container w-100 mx-auto">
          <Title className="lg:mb-10">{t("#4-title")}</Title>
          <div className="lg:flex justify-center items-center">
            <div className="lg:w-2/4 text-gray-800 lg:mr-10  mb-5 lg:mb-0">
              {t("#4-content")}
            </div>
            <div className="lg:w-1/4  ">
              <NewsletterSubscriptionForm />
            </div>
          </div>
        </div>
      </Section>
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const firstThirdPosts = getSortedPostsData().slice(0, 3);
  const t = await getT(locale, "index");
  return {
    props: {
      firstThirdPosts,
      home: true,
      description: t("description"),
      image: "banner.jpg",
    },
  };
};
export default Home;
