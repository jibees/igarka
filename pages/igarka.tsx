import { GetStaticProps } from "next";
import PageHeader from "../components/PageHeader";
import Section from "../components/Section";
import { Title } from "../components/home";
import ImageWithModal from "../components/ImageWithModal";
import useTranslation from "next-translate/useTranslation";
import getT from "next-translate/getT";
import Trans from "next-translate/Trans";

const Projet = () => {
  const { t } = useTranslation("igarka");
  return (
    <>
      <PageHeader
        title={t("title")}
        description={t("description")}
      ></PageHeader>
      <Section>
        <div className="lg:flex lg:align-middle">
          <div className="lg:w-1/3">
            <Title>{t("section1_title")}</Title>
            <div className="lg:mt-10 mb-10 lg:mb-0 w-3/4 mx-auto ">
              <ImageWithModal name="igarka_town.jpg"></ImageWithModal>
            </div>
          </div>
          <div className="lg:w-2/3 lg:pl-20 lg:mt-10 text-gray-800 text-justify">
            <Trans i18nKey="igarka:section1_content" components={[<p />]} />
          </div>
        </div>
      </Section>
      <Section>
        <div className="lg:flex lg:flex-row-reverse lg:align-middle">
          <div className="lg:w-1/3">
            <Title>{t("section2_title")}</Title>
            <div className="lg:mt-10 mb-10 lg:mb-0 w-3/4 mx-auto ">
              <ImageWithModal name="ienissei.jpeg"></ImageWithModal>
            </div>
          </div>
          <div className="lg:w-2/3 lg:pr-20 lg:mt-10 text-gray-800 text-justify">
            <Trans i18nKey="igarka:section2_content" components={[<p />]} />
          </div>
        </div>
      </Section>
      <Section>
        <div className="lg:flex lg:align-middle">
          <div className="lg:w-1/3">
            <Title>{t("section3_title")}</Title>
            <div className="lg:mt-10 mb-10 lg:mb-0 w-3/4 mx-auto ">
              <ImageWithModal name="igarka_socio.jpg"></ImageWithModal>
            </div>
          </div>
          <div className="lg:w-2/3 lg:pl-20 lg:mt-10 text-gray-800 text-justify">
            <Trans i18nKey="igarka:section3_content" components={[<p />]} />
          </div>
        </div>
      </Section>
      <Section>
        <Title>{t("section4_title")}</Title>
        <div className="text-gray-800 lg:w-1/2 lg:mx-auto text-justify mt-10">
          <Trans
            i18nKey="igarka:section4_content"
            components={[
              <p />,
              <p className="mt-5"></p>,
              <ul className="list-disc list-inside pl-10"></ul>,
              <li />,
            ]}
          />
        </div>
      </Section>
      <Section>
        <div className="lg:flex lg:align-middle">
          <div className="lg:w-1/3">
            <Title>{t("section5_title")}</Title>
          </div>
          <div className="lg:w-2/3 lg:pr-20 lg:mt-10 text-gray-800 text-justify">
            <Trans i18nKey="igarka:section5_content" components={[<p />]} />
          </div>
        </div>
      </Section>
      <Section>
        <div className="lg:flex lg:flex-row-reverse lg:align-middle">
          <div className="lg:w-1/3">
            <Title>{t("section6_title")}</Title>
          </div>
          <div className="lg:w-2/3 lg:pr-20 lg:mt-10 text-gray-800 text-justify">
            <Trans i18nKey="igarka:section6_content" components={[<p />]} />
          </div>
        </div>
      </Section>
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const t = await getT(locale, "igarka");
  return {
    props: {
      home: false,
      title: t("title"),
      description: t("description"),
    },
  };
};
export default Projet;
