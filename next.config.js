const withPlugins = require("next-compose-plugins");
const withReactSvg = require("next-react-svg");
const path = require("path");
const nextTranslate = require("next-translate");

const localeSubpaths = {};
const config = {
  include: path.resolve(__dirname, "./assets/svg"),
  webpack(config, options) {
    return config;
  },
};

module.exports = withPlugins([[withReactSvg, {}], [nextTranslate]], config);
