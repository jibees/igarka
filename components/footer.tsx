import NewsletterSubscriptionForm from "./NewsletterSubscriptionForm";
import useTranslation from "next-translate/useTranslation";

const Footer = ({ displayNewsletter }) => {
  const { t } = useTranslation("common");
  return (
    <footer className="bg-gray-300 p-8 w-full">
      <div className="sm:flex mb-4 container mx-auto text-gray-600 content-center justify-center">
        <div className="sm:w-1/2 h-auto">
          <div className="leading-normal text-center">
            <div className="font-semibold">{t("footer_title")}</div>
            <p>{t("footer_description")}</p>
            <div className="text-sm mt-5">
              {t("footer_photos")}
              <a
                href="http://www.arnaud-mansat.fr/"
                className="hover:text-green-800"
              >
                Arnaud Mansat
              </a>
            </div>
          </div>
        </div>

        {displayNewsletter && (
          <div className="sm:w-1/2 h-auto">
            <div className="text-green-600 mb-2">{t("newsletter_title")}</div>
            <p className="leading-normal">{t("newsletter_description")}</p>
            <div className="mt-4 mb-3 ">
              <NewsletterSubscriptionForm />
            </div>
          </div>
        )}
      </div>
    </footer>
  );
};

export default Footer;
