import { parseISO } from "date-fns";

const options = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric",
};

const shortOptions = {
  ...options,
  weekday: "short",
  month: "2-digit",
};

export default function Date({
  dateString,
  lang,
  short = false,
}: {
  dateString: string;
  lang: string;
  short?: boolean;
}) {
  const date = parseISO(dateString);
  return (
    <time dateTime={dateString}>
      {date.toLocaleDateString(lang, short ? shortOptions : options)}
    </time>
  );
}
