import { ReactNode } from "react";

export const Title = ({
  className,
  children,
}: {
  className?: string;
  children: ReactNode;
}) => (
  <div className={className}>
    <div className="lg:text-3xl lg:mb-0 mb-5 text-2xl text-gray-600 text-center">
      {children}
    </div>
    <div className="w-1/5 h-px bg-green-800 opacity-75 rounded-lg mx-auto lg:visible invisible" />
  </div>
);

export const ThirdComponent = ({
  icon,
  children,
}: {
  icon?: ReactNode;
  children: ReactNode;
}) => (
  <div className="lg:w-3/4 mx-auto h-auto my-6 lg:mx-8 mt-5 lg:mt-0">
    <div className="text-center text-xl my-2 w-2/12 mx-auto mb-5">{icon}</div>
    <div className="text-justify mx-4 text-gray-800 h-32">{children}</div>
  </div>
);
