import Head from "next/head";
import Nav from "./nav";
import Footer from "./footer";
import { useRouter } from "next/router";
import { getAbsoluteURL } from "../lib/urls";
export const siteTitle = "Projet Igarka";

export default function Layout({
  children,
  home,
  title,
  description,
  image,
}: {
  children: React.ReactNode;
  home?: boolean;
  title: string;
  description: string;
  image: string;
}) {
  const mainTitle = home ? siteTitle : siteTitle + " | " + title;
  const router = useRouter();
  const currentURL = getAbsoluteURL(router.asPath).toString();
  const openGraph: OpenGraph = {
    title: home ? siteTitle : title + " | " + siteTitle,
    description: description,
    image: image ? image : "banner.jpg",
  };
  const ogImage = `../public/images/${openGraph.image}?resize&size=600`;
  return (
    <>
      <Head>
        <title>{mainTitle}</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" content={openGraph.description} />
        {/* Twitter */}
        <meta name="twitter:card" content="summary_large_image" key="twcard" />
        {/* Open Graph x */}
        <meta property="og:url" content={currentURL} key="ogurl" />
        <meta property="og:title" content={openGraph.title} key="ogtitle" />
        <meta
          property="og:description"
          content={openGraph.description}
          key="ogdesc"
        />
        <meta
          property="og:image"
          content={getAbsoluteURL(ogImage).toString()}
          key="ogimage"
        />
        <meta property="og:site_name" content={siteTitle} key="ogsitename" />
      </Head>

      <div id="overlay-root"></div>
      <div id="modal-root"></div>

      <Nav />
      <main className="min-h-screen">{children}</main>
      <Footer displayNewsletter={!home} />
    </>
  );
}
