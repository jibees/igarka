import { Title } from "./home";
import { ReactNode } from "react";
import Image from "next/image";

const PageHeader = ({
  title,
  description,
  children,
}: {
  title: string;
  description?: string;
  children?: ReactNode;
}) => (
  <header className="w-full bg-white relative ">
    <div className="z-20 relative text-white container mx-auto p-5 lg:p-10">
      <div className="text-3xl lg:text-4xl xl:text-5xl lg:mb-0 mb-5  text-gray-600 text-center text-s font-semibold tracking-tighter ">
        {title}
      </div>
      <div className="lg:w-2/3 sm:3/4 mx-auto lg:mt-10 text-gray-700 font-light text-justify leading-5">
        {description}
      </div>
    </div>
    <div className="absolute inset-0 h-auto z-10">
      <Image
        layout="fill"
        objectFit="cover"
        className="opacity-50"
        src="/images/banner.jpg"
      />
    </div>
    {children}
  </header>
);

export default PageHeader;
