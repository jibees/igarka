import { useRef, useState } from "react";
import { useMouseEvents } from "beautiful-react-hooks";
import classNames from "classnames";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

export default function Nav({ home }: { home?: boolean }) {
  const { t, lang } = useTranslation("common");
  const elementsLeft: NavElement[] = [
    {
      label: t("nav_trajectoires_title"),
      title: t("nav_trajectoires_description"),
      href: "/les-spheres-de-recherche",
    },
    {
      label: t("nav_igarka_title"),
      title: t("nav-igarka_description"),
      href: "/igarka",
    },
    {
      label: t("nav_membres_title"),
      title: t("nav_membres_description"),
      href: "/membres",
    },
  ];
  const elementsRight: NavElement[] = [
    {
      label: t("nav_processus_title"),
      title: t("nav_processus_description"),
      href: "/le-processus",
    },
    {
      label: t("nav_blog_title"),
      title: t("nav_blog_description"),
      href: "/blog",
    },
    /*
    {
      label: t("nav_art_title"),
      title: t("nav_art_description"),
      href: "/art",
    },
    */
    {
      label: t("nav_soutiens_title"),
      title: t("nav_soutiens_description"),
      href: "/nos-soutiens",
    },
  ];
  return (
    <div className="w-full mx-auto flex flex-wrap bg-green-700 p-6 flex-col md:flex-row items-center text-white">
      <nav className="flex xl:w-2/5 flex-wrap items-center text-base md:ml-auto justify-center xl:justify-start md:justify-end">
        {elementsLeft.map(elementToNavElementMapper)}
      </nav>
      <Link href="/">
        <a className="flex order-first xl:order-none xl:w-1/5 title-font font-medium items-center xl:items-center xl:justify-center mb-4 md:mb-0">
          <span className="font-semibold text-2xl tracking-tight transition-transform duration-200 ease-in-out transform hover:translate-y-1 hover:translate-x-1">
            {t("nav_title")}
          </span>
        </a>
      </Link>
      <div className="xl:w-2/5 inline-flex xl:justify-end ml-5 xl:ml-0">
        {elementsRight.map(elementToNavElementMapper)}
      </div>
    </div>
  );
}

const elementToNavElementMapper = ({ href, title, label, submenu }) => {
  const id = "key_" + label;
  return (
    <NavElement
      key={id}
      id={id}
      title={title}
      href={href}
      label={label}
      submenu={submenu}
    />
  );
};

/*
 * Submenu is still TODO
 */

const NavElement = ({ id, title, href, label, submenu }) => {
  const ref = useRef();
  const { onMouseEnter, onMouseLeave } = useMouseEvents(ref);
  const [showSubmenu, setShowmenu] = useState(false);
  onMouseEnter((e) => setShowmenu(true));
  onMouseLeave((e) => setShowmenu(false));
  return (
    <div className="relative inline-block" ref={ref}>
      <NavElementLabel
        id={id}
        title={title}
        href={href}
        label={label}
        hasSubmenu={submenu != null}
      />
      {submenu && (
        <SubNavElementsWrapper key={id} show={showSubmenu}>
          {submenu.map(({ href, title, label }) => (
            <SubNavElement label={label} href={href} />
          ))}
        </SubNavElementsWrapper>
      )}
    </div>
  );
};

const NavElementLabel = ({ id, title, href, label, hasSubmenu }) => (
  <a
    id={"options-menu" + id}
    key={id}
    title={title}
    href={href}
    className="block mt-4 xl:inline-block xl:mt-0 text-teal-200 hover:text-white mr-4 transition ease-in-out duration-150"
  >
    {label}
  </a>
);

const SubNavElementsWrapper = ({ show, key, children }) => {
  const wrapperClassnames = classNames(
    "origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg transition ease-in-out duration-150",
    {
      visible: show,
      invisible: !show,
    }
  );
  return (
    <div className={wrapperClassnames}>
      <div className="rounded-md bg-white shadow-xs">
        <div
          className="py-1"
          role="menu"
          aria-orientation="vertical"
          aria-labelledby={"options-menu" + key}
        >
          {children}
        </div>
      </div>
    </div>
  );
};
const SubNavElement = ({ title, label, href }: BaseNavElement) => (
  <a
    href={href}
    className="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
    role="menuitem"
  >
    {label}
  </a>
);
