import Date from "../date";
import Link from "next/link";
import Image from "next/image";
import useTranslation from "next-translate/useTranslation";

export const PostAsCard = (post: SummaryPost) => {
  const { t, lang } = useTranslation();
  return (
    <Link href={`/blog/${post.id}`}>
      <div className="md:flex md:mb-10 mb-5 bg-white shadow md:shadow-lg rounded-lg cursor-pointer">
        <div className=" md:w-1/3">
          <div className="bg-gray-500 h-full relative md:pb-100 pb-9/16 w-full md:rounded-l-lg md:rounded-none rounded-t-lg">
            {post.image && (
              <Image
                layout="fill"
                objectFit="cover"
                className="absolute shadow-lg md:rounded-l-lg md:rounded-none rounded-t-lg"
                src={`/images/${post.image}`}
              />
            )}
          </div>
        </div>
        <div className="p-5 pt-2 md:w-2/3">
          <div className="text-lg truncate text-gray-700 hover:text-gray-900">
            {post.title}
          </div>
          <div className="text-gray-700 text-xs text-right font-thin leading-normal italic">
            <Date dateString={post.date} lang={lang} short={true} />
          </div>
          <div className="text-sm text-gray-700 text-justify mt-5 truncate-5-lines">
            {post.summary}
          </div>
        </div>
      </div>
    </Link>
  );
};
