import { useState, useEffect } from "react";
import classNames from "classnames";
import useTranslation from "next-translate/useTranslation";

const NewsletterSubscriptionForm = () => {
  const { t } = useTranslation("common");

  const [email, setEmail] = useState("");
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const [isSuccessVisible, setSuccessVisibility] = useState(null);
  const [submitButtonLabel, setSubmitButtonLabel] = useState(
    t("newsletter_subscribe")
  );
  const [isSubmitting, setSubmitting] = useState(false);

  const checkForm = (): boolean => {
    if (email) {
      return true;
    }
    setError(t("newsletter_error_email"));
    return false;
  };
  const submitForm = async (evt) => {
    evt.preventDefault();
    setError(null);
    if (!checkForm()) {
      return;
    }
    if (isSubmitting) {
      return;
    }
    setSubmitButtonLabel(t("newsletter_submitting"));
    setSubmitting(true);

    const res = await fetch("/api/subscribe", {
      body: JSON.stringify({
        email: email,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
    });

    const response = await res.json();
    if (response.status != 200) {
      setError(response.message);
      setSubmitButtonLabel(t("newsletter_subscribe"));
      setSubmitting(false);
    } else {
      setEmail("");
      setSuccess(t("newsletter_sucess"));
      setSuccessVisibility(true);
      setSubmitButtonLabel(t("newsletter_subscribe"));
      setTimeout(() => {
        setSuccessVisibility(false);
        setTimeout(() => setSuccess(null), 1000);
        setSubmitting(false);
      }, 3000);
    }
  };
  const classes = classNames(
    "text-green-800 text-sm font-bold transition-opacity duration-1000 ease-in-out",
    {
      "opacity-100": isSuccessVisible,
      "opacity-0": !isSuccessVisible,
    }
  );
  return (
    <>
      <div className="flex">
        <input
          type="text"
          className="p-2 border border-grey-light round text-gray-900 text-sm h-auto "
          placeholder={t("newsletter_placeholder")}
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
            setError(null);
          }}
        />
        <button className="button button-stacked-form" onClick={submitForm}>
          {submitButtonLabel}
        </button>
      </div>
      <div className="h-4 mt-2">
        {error && <p className="text-red-500 text-xs italic">{error}</p>}
        <p className={classes}>{success}</p>
      </div>
    </>
  );
};

export default NewsletterSubscriptionForm;
