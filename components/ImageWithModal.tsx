import { useState, useRef, useEffect, useCallback } from "react";
import { createPortal } from "react-dom";
import Transition from "./Transition";
import useOnClickOutside from "../hooks/useOnClickOutside";
import Image from "next/image";

const ImageWithModal = ({ name }) => {
  const [showModal, setShowModal] = useState(false);
  const ref = useRef(null);
  useOnClickOutside(ref, () => setShowModal(false));
  const escFunction = useCallback((event: KeyboardEvent) => {
    if (event.keyCode === 27) {
      setShowModal(false);
    }
  }, []);
  useEffect(() => {
    document.addEventListener("keydown", escFunction, false);
    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  }, []);
  return (
    <>
      <div
        className="mx-auto flex flex-col items-center center-items relative h-64 scale-on-hover"
        onClick={() => setShowModal(true)}
      >
        <Image
          layout="fill"
          objectFit="cover"
          className="shadow-lg "
          src={`/images/${name}`}
        />
      </div>

      <>
        <ClientOnlyPortal selector="#overlay-root">
          <Transition
            show={showModal}
            enter="transition-opacity ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition ease-out duration-100"
            leaveFrom="opacity-100 "
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 opacity-50 bg-black z-50"></div>
          </Transition>
        </ClientOnlyPortal>

        <ClientOnlyPortal selector="#modal-root">
          <Transition
            show={showModal}
            enter="transition-opacity ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition ease-out duration-100"
            leaveFrom="opacity-100 "
            leaveTo="opacity-0"
          >
            <div
              className="fixed transition inset-10 p-4 bg-white mx-auto flex-col flex z-50 rounded shadow-lg overflow-y-auto"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
              ref={ref}
            >
              <Image layout="fill" objectFit="cover" src={`/images/${name}`} />
            </div>
          </Transition>
        </ClientOnlyPortal>
      </>
    </>
  );
};

const Overlay = () => {};

const ClientOnlyPortal = ({ children, selector }) => {
  const ref = useRef();
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    ref.current = document.querySelector(selector);
    setMounted(true);
  }, [selector]);

  return mounted ? createPortal(children, ref.current) : null;
};

export default ImageWithModal;
