import { capitalize } from "../../lib/utils";

export const getFullName = ({ firstName, lastName }) =>
  `${capitalize(firstName)} ${capitalize(lastName)}`;

export const getAvatarPath = (member: ProjectMember): string => {
  let avatarPath = null;
  avatarPath = `/images/members/${member.id}.jpg`;
  return avatarPath;
};
