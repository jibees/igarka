import Link from "next/link";
import { getFullName, getAvatarPath } from "./util";
import Image from "next/image";

const MemberAsCard = (member: ProjectMember) => {
  return (
    <div
      className="flex flex-col items-center justify-center bg-white p-4 shadow rounded-lg"
      key={member.id}
    >
      <div className="inline-flex shadow-lg border border-gray-200 rounded-full overflow-hidden h-40 w-40 relative ">
        <Link href="/membres/[id]" as={`/membres/${member.id}`} key={member.id}>
          <a>
            <Image
              layout="fill"
              src={getAvatarPath(member)}
              alt=""
              className="w-full object-contain h-48 transform scale-on-hover"
            />
          </a>
        </Link>
      </div>

      <Link href="/membres/[id]" as={`/membres/${member.id}`} key={member.id}>
        <a>
          <h2 className="mt-4 font-bold text-xl text-gray-900 hover:text-gray-600">
            {getFullName(member)}
          </h2>
        </a>
      </Link>
      <h6 className="mt-2 text-sm text-center text-gray-600">{member.job}</h6>

      <p className="text-xs text-gray-600 text-justify mt-3">
        {member.summary}
      </p>
    </div>
  );
};
export default MemberAsCard;
