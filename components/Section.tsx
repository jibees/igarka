const Section = ({ children }) => (
  <section className="w-full bg-white even:bg-gray-100 shadow-inner">
    <div className="max-w-screen-xl mx-auto p-5 md:p-10 lg:p-20 xl:px-0">
      {children}
    </div>
  </section>
);
export default Section;
