declare module "remark-html" {
  const html: any;
  export default html;
}

type NavElement = BaseNavElement & {
  submenu?: BaseNavElement[];
};

type BaseNavElement = {
  href: string;
  label: string;
  title?: string;
};

type SummaryPost = {
  id: string;
  title: string;
  date: string;
  image?: string;
  summary?: string;
};

type Post = SummaryPost & {
  contentHtml: string;
};

type ProjectMember = {
  id: string;
  firstName: string;
  lastName: string;
  job: string;
  summary: string;
  content: string;
  twitter?: string;
  linkedin?: string;
  link?: string;
};

declare module "*.svg" {
  const content: any;
  export default content;
}

type OpenGraph = {
  title: string;
  description: string;
  image: string;
};
