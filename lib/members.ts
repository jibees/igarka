import fs from "fs";
import path from "path";
import matter from "gray-matter";
import remark from "remark";
import html from "remark-html";

const membersDirectory = path.join(process.cwd(), "/data/members");

export function getProjectMembers(): ProjectMember[] {
  // Get file names unde
  const fileNames = fs.readdirSync(membersDirectory);
  const members = fileNames.map((fileName) => {
    // Remove ".md" from file name to get id
    const id = fileName.replace(/\.md$/, "");

    // Read markdown file as string
    const fullPath = path.join(membersDirectory, fileName);
    const fileContents = fs.readFileSync(fullPath, "utf8");

    // Use gray-matter to parse the post metadata section
    const matterResult = matter(fileContents);

    // Combine the data with the id
    return {
      id,
      content: matterResult.content,
      ...(matterResult.data as {
        lastName: string;
        firstName: string;
        job: string;
        summary: string;
        twitter?: string;
        linkedin?: string;
        link?: string;
      }),
    };
  });
  // Sort members by lastName
  return members.sort((a, b) => {
    return ("" + a.lastName).localeCompare(b.lastName);
  });
}

export function getAllProjectMembersId() {
  const fileNames = fs.readdirSync(membersDirectory);
  return fileNames.map((fileName) => {
    return {
      params: {
        id: fileName.replace(/\.md$/, ""),
      },
    };
  });
}

export async function getMemberData(id: string) {
  const fullPath = path.join(membersDirectory, `${id}.md`);
  const fileContents = fs.readFileSync(fullPath, "utf8");

  // Use gray-matter to parse the post metadata section
  const matterResult = matter(fileContents);

  // Use remark to convert markdown into HTML string
  const processedContent = await remark()
    .use(html)
    .process(matterResult.content);
  const contentHtml = processedContent.toString();

  // Combine the data with the id and content
  return {
    id,
    content: contentHtml,
    ...(matterResult.data as {
      firstName: string;
      lastName: string;
      job: string;
      summary: string;
    }),
  };
}
