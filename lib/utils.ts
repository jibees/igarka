export const capitalize = function (s: string) {
  return s.charAt(0).toUpperCase() + s.slice(1);
};
