export const getAbsoluteURL = (path: string): URL => {
  const baseURL = process.env.VERCEL_URL
    ? `https://${process.env.VERCEL_URL}`
    : "http://localhost:3000";
  return new URL(path, baseURL);
};
