---
title: "Rencontre du 19 novembre"
date: "2020-11-19"
summary: "Dans cette année de grande incertitude, nous avons réussi à nous retrouver en distanciel uniquement : mais avec des échanges passionants !"
image: "blog/19-novembre.jpg"
---

Dans cette année de grande incertitude, organiser une rencontre en présentielle était une gageure. Nous n'avons pas réussi, mais nous nous sommes adaptés et avons organisé une rencontre via visioconférence avec un programme passionant :

- <small>9h30 - 10h00</small> **_Roman_** : accueil, présentation, où en est le projet ?
- <small>10h00 - 11h30</small> **_Jean-Yves_** : Réflexivité et réflexion autour de l'interdisciplinarité
- <small>11h30 - 12h00</small> Debrief de la matinée et perspectives du projet
- <small>13h30 - 14h15</small> **_Béatrice_** : Enseignement en éthique et recherche en communication, des interactions écopositives
- <small>14h15 - 15h00</small> **_Vanessa_** : Regards sur les trajectoires de l'Anthropocène
- <small>15h00 - 16h00</small> Perspectives et planification de l'année 2021

Le compte rendu de notre rencontre est disponible [ici](https://docs.google.com/document/d/1fCHYWU1m7Y28EWxwIOIsfd_DptjB7CBCROdUqkBm8_w/edit?usp=sharing).
