---
firstName: "Vincent"
lastName: "Gerbaud"
job: "Directeur de recherche - Génie des procédés"
summary: ""
link: "https://lgc.cnrs.fr/annuaire/vincent-gerbaud/"
---

Je mène des recherches en ingénierie des systèmes de procédés, en mettant l'accent sur la modélisation à petite échelle : simulation moléculaire, thermodynamique, conception moléculaire et de mélanges assistée par ordinateur. J'ai commencé à étudier la crème de tartre en cristaux et j'aimerais partager mon expertise avec tous ceux qui veulent goûter ! À l'échelle des procédés, je m'occupe de la conception et de la synthèse de procédés de distillation extractive ou azéotropique. Je m'interroge sur leur conception optimale à la lumière de la thermodynamique du mélange avec système d'entraînement, qui a abouti à un critère général de faisabilité de la conception. Cela a été possible, d'une part, grâce à l'étude de la thermodynamique des processus d'équilibre de phase, à la fois en théorie, dans les modèles et avec la simulation moléculaire et, d'autre part, grâce au développement d'outils logiciels de conception de produits et de processus assistée par ordinateur. Un sujet lié à la thermodynamique d'équilibre a été la prédiction des points d'éclair des mélanges et la mise au point d'une classification générale des mélanges de points d'éclair.

Récemment, à l'aide des principes de la thermodynamique hors équilibre et de la théorie de "la pensée complexe" d'E. Morin, j'ai abordé les problèmes avec une approche holistique, en les considérant comme des systèmes ouverts avec des exigences techniques dans le cadre de questions socio-économiques. Cela implique toujours des équipes interdisciplinaires de sociologues, géochimistes, économistes, physiciens, et concerne des systèmes à grande échelle, qu'il s'agisse d'un parc d'énergie renouvelable, d'un système de décision à l'échelle de l'entreprise, d'un écosystème sibérien, d'un système macroéconomique territorial ou autre. Peu de publications, mais beaucoup d'idées. Contactez-moi
