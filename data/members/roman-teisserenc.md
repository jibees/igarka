---
firstName: "Roman"
lastName: "Teisserenc"
job: "Maître de Conférences - Biogéochimie de l'environnement"
summary: ""
---

Pur produit des montagnes cévenoles, j’ai migré à Montréal au Canada où j’ai réalisé la quasi totalité de mes études universitaires. De formation initiale en Biologie Ecologie, j’ai par la suite été formé aux sciences de l’environnement, avec une analyse transdisciplinaire des systèmes économiques, sociaux et naturels…
