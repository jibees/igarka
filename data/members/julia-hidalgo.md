---
firstName: "Julia"
lastName: "Hidalgo"
job: "Chercheuse - Climat Urbain"
summary: ""
link: "https://blogs.univ-tlse2.fr/julia-hidalgo/"
---

Après une formation universitaire en physique de l’atmosphère et l’environnement (1998-2004) je me suis spécialisé en climatologie et météorologie urbaines via une thèse Européenne binationale France-Espagne (2005-2008). Pendant la période 2008 à 2012, en tant que chercheur contractuel, j’ai élargi mon champ de recherche aux volets d’impact et d’adaptation des villes au changement climatique. Je peux donc apporter les entrées disciplinaires qui relèvent de l’observation et la modélisation du climat global et du climat urbain.

J‘ai été recrutée en 2013 par la Commission interdisciplinaire 52 du CNRS sur un poste intitulé « Risques Climatiques : approches modélisateurs » avec une affectation dans le laboratoire LISST, équipe CIEU (Centre Interdisciplinaire d’Études Urbains). Mon projet de recherche actuel vise à proposer des modes d’articulation entre savoirs climatiques et savoirs urbanistiques en ayant recours aux observations, à la modélisation, et à l’analyse des outils de planification et d’aménagement urbains.
