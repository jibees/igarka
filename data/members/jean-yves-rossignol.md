---
firstName: "Jean-Yves"
lastName: "Rossignol"
job: "Ingénieur-conseil, enseignant et chercheur indépendant"
summary: ""
link: ""
---

Jean-Yves Rossignol, ingénieur-conseil, enseignant et chercheur indépendant, médiateur interdisciplinaire, travaille à l’émergence d’une conscience éthique active dans les univers de l’entreprise, de la recherche et de l’enseignement supérieur. Il est membre du conseil d’administration du Réseau Intelligence de la Complexité, réseau européen de recherche et d’échanges sur les sciences de la complexité.
